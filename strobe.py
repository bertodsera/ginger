#!/usr/bin/env python3
#
# Apply a strobe light to a film
#
# Berto 4 November 2019
#
# version: 0.1.1


import sys
import ffmpeg
from ffmpy import FFmpeg
import argparse

if __name__ == '__main__':

  # parse received command 
  parser = argparse.ArgumentParser(description='Apply a strobe light to a video file')
  parser.add_argument('filename', 
    metavar='filename', 
    help='the video file you want to apply the strobe light to')
  parser.add_argument('--bpm', default='60', type=int,
    help='number of flashes per minute. Default 60')
  parser.add_argument('--frames', default='1', type=int,
    help='number of consecutive visible frames. Default 1')  
  
  args = parser.parse_args()
  requested = vars(args)
  
  # test the video file
  fileProbe  = ffmpeg.probe(requested['filename'])
    
  # Compute dimensions of the frame
  width  = str(fileProbe['streams'][0]['width'])
  height = str(fileProbe['streams'][0]['height']) 
  
  
  # compute the position of the light flash
  frameRate  = int(fileProbe['streams'][0]['r_frame_rate'].split('/')[0])
  lightFrame = int(frameRate*60/int(requested['bpm']))
  
  # Compute light frames enabler command
  enabler = ""
  nFrames = int(requested['frames'])
  atom    = "mod(n+{}\,"+str(lightFrame)+")"
  for x in range(nFrames):
    enabler = enabler + '*' + atom.format(x)
  enabler = enabler.replace('*', '', 1)  

  # execute
  ff = FFmpeg(
    inputs  = {requested['filename'] : None},
    outputs = { 'strobe.mp4' : '-vf "drawbox=x=0:y=0:w={}:h={}:color=black:t=999:enable=({})" '.format(width,height,enabler)}
  )
  ff.run()  
