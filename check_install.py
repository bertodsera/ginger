#!/usr/bin/env python3
#
# check installation
#
# Berto 1 November 2019

# version: 0.1.1


import subprocess
import sys

def is_tool(name):
    """Check whether `name` is on PATH and marked as executable."""

    # from whichcraft import which
    from shutil import which

    return which(name) is not None

needed = [ 'ffmpeg-python', 'ffmpy' ]

reqs = subprocess.check_output([sys.executable, '-m', 'pip', 'freeze'])
installed_packages = [r.decode().split('==')[0] for r in reqs.split()]

good2go = True

# Check python packages
for package in needed:
  if not package in installed_packages:
    print("You still need to install "+package)
    good2go = False
    
# check ffmpeg
if not is_tool('ffmpeg'):
    print("You still need to install ffmpeg")
    good2go = False
    
if good2go:
  print("Your installation is complete!")    
