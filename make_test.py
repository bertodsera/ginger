#!/usr/bin/env python3
#
# Prepare test project
#
# Berto 1 November 2019

# version: 0.1.1


from shutil import copytree

projectName = "Test"
projectPath = "./Projects/" + projectName
mediapath   = "./media/"

copytree(mediapath+projectName, "./Projects/"+projectName)
