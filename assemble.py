#!/usr/bin/env python3
#
# Assemble film from raw scenes
#
# Berto 23 Maj 2019

# TODO flags for:
#   1) technicolour
#   2) BW
#   3) colour grading (with sample frames output)
#
# version: 0.1.1


import sys
import ffmpeg
from ffmpy import FFmpeg
from collections import OrderedDict
import os
import shutil
import argparse
from pathlib import Path


# The actual FFMPEG chain
def _crossfade(intakeReel, reelLength, intakepixfmt, sceneFilm, scenepixfmt, scenecount, outReelName, intakeDAR, scaleTo):
  global requested  
  transition = 1 # lenght in seconds
    
  # Number the scenes if requested
  if requested['document']: 
     numberOverlay = (
     ",drawtext=fontfile=./media/font/FoglihtenNo03.otf:text={}:x='w-tw-50':y='h-th-50':fontsize=96:fontcolor=white".format(scenecount)    
     )
  else:
     numberOverlay = "" 
  
  # Audio overlay chain
  if requested['mute']: 
     audioMergeInFilter = ""
  else:
     audioMergeInFilter = (";" +
     "[0:a]afade=t=out:st={}:d={}[a0];".format((reelLength-transition),transition) +
     "[1:a]afade=t=in:st=0:d={}[a1];".format(transition) +
     "aevalsrc=0:d={}[s1];".format(reelLength)  +
     "[s1][a1]concat=n=2:v=0:a=1[ac1];" +
     "[a0][ac1]amix[a]"
     )
  
  # Video overlay chain (with added audio chain, if so is required)
  command = ( 
  "[0:v]format=pix_fmts={},fade=t=out:st={}:d={}:alpha=1,setpts=PTS-STARTPTS[va0];".format(intakepixfmt,reelLength,transition) +
  "[1:v]format=pix_fmts={},fade=t=in:st=0:d={}:alpha=1,setpts=PTS-STARTPTS+{}/TB{}[va1];".format(scenepixfmt,transition,reelLength,numberOverlay) + 
  "[2:v]setdar=ratio={}:max=1000,scale={},trim=duration={}[over];".format(intakeDAR, scaleTo, reelLength) + 
  "[over][va0]overlay[over1];" + 
  "[over1][va1]overlay=format=yuv420[outv]{}".format(audioMergeInFilter) )
  
  # merge sections for video and optional audio
  videoMergeOutput = "-vcodec libx264 -map [outv]"
  if requested['mute']: 
     audioMergeOutput = ""
  else:  
     audioMergeOutput = "-map [a] -c:a aac -ac 2 -b:a 128k"
     
  # Assemble and execute the full chain   
  ff = FFmpeg(
    inputs  = OrderedDict([(intakeReel, None), (sceneFilm, None), ('color=black', '-f lavfi')]),
    outputs = { outReelName : '-filter_complex \" {} \" {} {}'.format(command, videoMergeOutput, audioMergeOutput)}
  )
  ff.run()          


# Higher level abstraction of the overlay cycle, with parameter preparation
def crossfade(intakeName, sceneName, scenecount, workpath):   
    # Intake reel, this is the part of the film that has already been assembled (get pixel format and duration)  
    reelProbe    = ffmpeg.probe(intakeName)
    reelLength   = int(float(reelProbe['format']['duration']))-1
    intakepixfmt = reelProbe['streams'][0]['pix_fmt']    
    intakeDAR    = reelProbe['streams'][0]['display_aspect_ratio'].replace(":", "/")  
    scaleTo      = str(reelProbe['streams'][0]['width']) + "x" + str(reelProbe['streams'][0]['height']) 
    
    # The incoming filmstrip (get explicit pixel format from the file)
    sceneProbe   = ffmpeg.probe(sceneName)
    sceneLength  = int(float(sceneProbe['format']['duration']))
    scenepixfmt  = sceneProbe['streams'][0]['pix_fmt']
    
    # The resulting assembled film will be in
    outReelName   = workpath + "film" + str(scenecount).zfill(3) + ".mp4" 
    outReelLength = reelLength + sceneLength

    print("Joining " + rawscene)
    _crossfade(intakeName, reelLength, intakepixfmt, sceneName, scenepixfmt, scenecount, outReelName, intakeDAR, scaleTo)
    
    return outReelName  


# Do all the OS level pre-checks and preparation
def preProcess(projectName):
  global projectPath, recipeName, workpath, mediapath
  
  projectPath = "./Projects/" + projectName
  recipeName  = projectPath + "/" + projectName + ".recipe"
  workpath    = projectPath + "/workpath/"
  mediapath   = "./media/"

  
  # Check directory exists, if not, create an empty one and exit
  if not os.path.isdir(projectPath):
    os.makedirs(projectPath)
    Path(recipeName).touch()
    print('Empty project \"{}\" created. Enjoy!'.format(projectName))
    sys.exit(1)
    
  # make sure a clean workpath is there  
  if os.path.isdir(workpath):
    shutil.rmtree(workpath)  
  os.makedirs(workpath)
  os.system('touch {}'.format(recipeName))  



if __name__ == '__main__':

  # parse received command 
  parser = argparse.ArgumentParser(description='Join single videos into one, with crossfade transitions')
  parser.add_argument('projectname', 
    metavar='projectdir', 
    help='a directory where the files will be kept, together with a recipe file')
  parser.add_argument('-d', '--document', 
    help='number the scenes in the resulting video',                  
    action='store_const', 
    const=True, 
    default=False)
  parser.add_argument('-m', '--mute', 
    help='drop sound signal while joining the videos, resulting in a soundless stream',                  
    action='store_const', 
    const=True, 
    default=False)  


  args = parser.parse_args()
  requested = vars(args)
  projectName = requested['projectname']
  
  # Prepare all global paths, check working paths etc
  preProcess(projectName)

  # load ordered list of scenes
  with open(recipeName) as f:
    content = f.readlines()
  # remove whitespace characters like `\n` at the end of each line
  content = [x.strip() for x in content] 
  # drop empty lines
  content = list(filter(None, content))
  
  # utility counter for filenames and scene numbers
  scenecount = 0

  # Intake reel, this is the part of the film that has already been assembled   
  intakeName = mediapath + "Ginger.mp4"
   
  # Cycle for all raw scenes, according to recipe
  for rawscene in content:
    scenecount  += 1 

    # The film strip we're adding at the end of the existing reel
    sceneName  = projectPath + "/" + rawscene   

    # join the two film strips and update the intakereel to the name of the resulting file
    intakeName = crossfade(intakeName, sceneName, scenecount, workpath)

  # Move out the final file and get rid of the workpath
  shutil.move(intakeName, projectPath + "/" + projectName + '_assembled.mp4')   
  shutil.rmtree(workpath) 




