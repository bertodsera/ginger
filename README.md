>  # Ginger

Ginger is a small toolset for videoediting. It is aimed to anyone who doesn't have a budget for an expensive computer, and also finds that running FFMPEG is way too complex for them. All it does is running FFMPEG for you, thus making the creation of very complex commands rather easy. It has NO GUI, this is a command line tool.

It should be ideally suited for people and schools that are teaching kids basic filming skills, as it can run with stuff everyone has anyway: smartphones and old PCs. It is written in python, and you can run it on Windows, Mac and Linux. 

The workflow is pretty simple: make the digital equivalent of strips of film (single files, that is) and glue them into a sequence. Just the plain old way. The system doesn't care how you make your strips, so you can shoot stuff from a smartphone, or come from a film scanner. As long as it's a file, it should work. 


**INSTALLATION**
============
Grab the compressed file (on the right here, close to "web IDE") and expand it on your computer. This you have to do anyway, no matter what OS you are running. If you are willing to contribute patches you will of course need to clone the repo in instead.

**Windows**

NOTE: you do this part only once, for successive releases of ginger you do not need to do it again.

* Install python3, from here: https://www.python.org/ftp/python/3.8.0/python-3.8.0-amd64-webinstall.exe (make sure you check the "add Python3.8 to PATH" box!)
* Install ffmpeg, from here: https://ffmpeg.zeranoe.com/builds/win64/static/ffmpeg-4.2.1-win64-static.zip (and follow this guide: https://www.wikihow.com/Install-FFmpeg-on-Windows)
* open the powershell (https://www.isunshare.com/windows-10/5-ways-to-open-windows-powershell-in-windows-10.html) and navigate to your ginger directory

Now execute the following:
* pip install ffmpeg-python
* pip install ffmpy
* python check_install.py
 
you should read that your installation is complete.

**Mac**

Sorry I currently have no mac at hand, will test as soon as possible

**Linux**

* sudo apt install ffmpeg python3 python3-pip
* pip install ffmpeg-python
* pip install ffmpy
* python check_install.py
 
you should read that your installation is complete.


**Let's use it!**
=============
NOTE: if you are on Linux, you don't need to put "python" in the command, the system is smart enough to know what to do. The following commands are tailored to Windows users.

**Prepare the test material**

> python make_test.py 

This will deploy the test files and prepare a basic project that you can use to test the commands

> python assemble.py Test -d

This will create a single video out of the 3 test files (just three colours, each for 10 seconds) and number the scenes. Once the command is finished, you will find the result in a file named Test_assembled.mp4. You can watch it and find out what the command did.

**Syntax**
=============
When in doubt type 
> python assemble.py -h 

this will produce a list of existing switches, with an explanation of what they do

**Create your own project!**
==========================
Choose a name for the project. No spaces, just a single word, with only letters and/or numbers, use that in place of PROJECTNAME in the following command
> python assemble.py PROJECTNAME

This will create a directory for your project, you will need to put your video files in it. It also creates an empty PROJECTNAME.recipe file. Open this file and put the names of your files, one per line, in the order in which you want to join them. 

